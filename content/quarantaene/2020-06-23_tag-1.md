Title: Tag 1: Hofgang und Workout
Date: 2020-06-23T19:50+02:00
Modified: 2020-06-23T19:50+02:00
Category: Quarantäne
Tags: 
Slug: tag-1
Authors: Stephan Kusch
Status: published

Um sechs Uhr ist die Nacht vorbei. Die Sonne geht auf der dem Fenster
abgewandten Seite auf und beleuchtet den hoch gelegenen Kottenforst. Typisches
Rheintal-Flair, erinnert mich ein wenig an Koblenz, nur grüner. Frühstück gibt
es erst um acht. Bis dahin werde ich mich zweimal selbst verdaut haben.

[Blick vom Hotelzimmer auf den Kottenforst][img1]

Das Frühstück kommt um 8:45 Uhr, nachdem ich mich _dreimal_ selbst verdaut habe.
Es ist reichlich, diesmal mit Butter anstelle veganer Margarine. Trotz
Beschriftung "laktosefrei" habe ich einen Joghurt bekommen, der muss leider
zurück gehen. Zum Frühstück läuft Marvel's Punisher auf Netflix.

[Frühstückstablett][img2]

Lebendigmeldung um 10:25 Uhr. Halb zwölf Abholung zum "Hofgang". Wir werden nach
unten geführt und können uns dann in einem relativ großen Areal um einen Flügel
des Hotels bewegen. Ich gehe laufen, mein Zimmernachbar begleitet mich, einige
andere laufen auch, man muss sich ausweichen. Als wir wieder oben sind, gibt mir
mein Zimmernachbar ein Blatt mit Workouts. Er ist in der Hinsicht gut
vorbereitet, will in den nächsten Wochen 15 Kilo abnehmen.

Das Mittagessen kommt um 12:45 Uhr. Danach fallen mir beim Lesen die Augen zu.

[Mittagessen][img3]

Nach einem kurzen Mittagschlaf raffe ich mich zu einem Bauchmuskel-Workout auf.
Ist nicht so ganz mein Fall. Das Abendessen ist relativ pünktlich um 18:15 da
und wieder ein Fest für Fleisch- und Wurstliebhaber. Mal sehen, wann mir der
Kragen platzt und ich Käse verlange.

[Abendessen][img4]

## Lektüre

+ [Unix: Ein Betriebssystem in 8 KByte][1]
+ [Abhijit Banerjee, Esther Duflo: Good Economics for Hard Times][2]

[img1]: {static}/images/2020-06-23_aussicht.jpg
[img2]: {static}/images/2020-06-23_mahlzeit_1.jpg
[img3]: {static}/images/2020-06-23_mahlzeit_2.jpg
[img4]: {static}/images/2020-06-23_mahlzeit_3.jpg
[1]: https://www.golem.de/news/unix-wird-50-die-wilde-jugend-von-unix-2006-149027.html
[2]: https://www.goodeconomicsforhardtimes.com

