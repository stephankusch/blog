Title: Tag 14: Die Bombe
Date: 2020-07-06T20:44+02:00
Category: Quarantäne
Tags: 
Slug: tag-14
Authors: Stephan Kusch
Status: published

Heute mache ich nichts körperlich anstrengendes, habe ich mir vorgenommen. Man
könnte es also Erholungs- oder gern auch Sumpftag nennen. Den Wecker habe ich
ignoriert und den Tag mit zwei Folgen Daredevil (auf Netflix) zum Frühstück
begonnen. Für den Hofgang habe ich mir strengen Kaffeegenuss verordnet. Und ich
werde mir wieder mehr Zeit zum Lesen nehmen, dafür habe ich mich vor der Abfahrt
ja extra mit Lektüre eingedeckt.

Das Stück Fleisch auf dem Teller ist übrigens ein vortrefflich medium-gebratenes
Steak mit ca. 150 g. Bestes [Mittagessen][img1] seit meiner Ankunft.

## Die Bombe

Die Bombe platzt bei einem kurzfristig anberaumten Briefing gegen 15 Uhr. Die
isolierte Unterbringung muss auf Geheiß der zuständigen Überwachungsstelle wegen
Mängeln in der Durchführung um eine Woche verlängert werden. Die Stimmung unter
den Anwesenden ist eher gereizt, es herrscht Unverständnis vor. Ich werde mich
an dieser Stelle nicht weiter dazu äußern, sondern lieber persönlich.

Ich hatte ja gehofft, dass ich das Zimmer nicht mehr reinigen muss, aber eine
weitere Woche in diesem Saustall ertrage ich nicht. :D

[img1]: {static}/images/2020-07-06_mahlzeit_1.jpg

