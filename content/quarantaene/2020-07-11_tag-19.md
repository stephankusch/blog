Title: Tag 19: Hier passiert heute nichts mehr
Date: 2020-07-11T17:38+02:00
Category: Quarantäne
Tags: 
Slug: tag-19
Authors: Stephan Kusch
Status: published

Seit gestern schaue ich wieder _Family Guy_, das dankenswerterweise auf Netflix
läuft. Ich hoffe, dass die Wände halbwegs schalldicht sind, damit die Nachbarn
sich nicht durch mein Gelächter gestört fühlen. Das hohe Maß an intelligentem
Fäkalhumor in dieser Serie ist erstaunlich. Ich rate dringend zur englischen
Originaltonspur.

In Deutschland werden offenbar vermehrt Goldschakale gesichtet. Die fehlende
Wolfspopulation der letzten hundert Jahre hat den Zuzug dieser possierlichen,
aber scheuen Raubtiere ebenso wie der langfristige Klimawandel begünstigt.
Spannend, dass die klassische Ökologie aus dem Biounterricht tatsächlich noch
funktioniert: Eine Art besetzt die Lücke in der ökologischen Nische, die eine
andere nach ihrer Ausrottung hinterlassen hat. Gehört im
[Deutschlandfunk Nova][7].

+ [Warmup][1]
+ [Workout][2]
+ [Stretching][3]

[Das Mittagessen][img1] macht mich seit langem wieder glücklich. Ballaststoffe! Die
kleinen frittierten Teilchen sind übrigens Champignons.

+ [Warmup][4]
+ [Workout][5]
+ [Stretching][6]

Ich habe nicht das Gefühl, dass hier und heute noch irgendetwas passiert. Am
Montag wird eine _Besinnung am Abend_ von der Seelsorge angeboten. Natürlich
werde ich teilnehmen. Diesmal gab es auf die Anmeldung per SMS tatsächlich eine
Reaktion. Sehr schön.

[img1]: {static}/images/2020-07-11_mahlzeit_1.jpg
[1]: https://darebee.com/workouts/classic-warmup-workout.html
[2]: https://darebee.com/workouts/size-down-workout.html
[3]: https://darebee.com/workouts/morning-stretch-workout.html
[4]: https://darebee.com/workouts/4-minute-warmup-workout.html
[5]: https://darebee.com/workouts/ab-hub-workout.html
[6]: https://darebee.com/workouts/rest-and-rec-workout.html
[7]: https://www.deutschlandfunknova.de

