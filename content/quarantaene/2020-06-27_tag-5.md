Title: Tag 5: Wochenende?
Date: 2020-06-27T19:43+02:00
Category: Quarantäne
Tags: 
Slug: tag-5
Authors: Stephan Kusch
Status: published

Der Unterschied zwischen Woche und Wochenende ist faktisch nicht existent. Das
einzige, was daran erinnert, ist, dass vormittags kein Frühstücks- sondern
Kinderfernsehen läuft. Warum soll man Kinder den ganzen Vormittag fernsehen
lassen? Genügt nicht die Sendung mit der Maus oder Löwenzahn?

Ich nehme mir wie gewohnt [ein Workout][2] vor. Der Himmel ist bedeckt, heute
Mittag soll es aber trocken bleiben, so dass ich hoffentlich ungehindert laufen
kann.

Das Workout haut rein. Das mag an den Burpees liegen oder der hohen
Luftfeuchtigkeit oder beidem.

[Das Mittagessen][img1] lässt sich als _Hausmannskost_ bezeichnen. Die kreative
Aufgabe ist heute, die Kiwi mit dem Messer zu schälen und zerteilen, da ein
kleiner Löffel zum klassischen Auslöffeln nicht mitgeliefert worden ist.
Letztens habe ich im Fernsehen gelernt, dass man die Schale prinzipiell mitessen
kann; da ich allerdings nicht weiß, ob diese Kiwi gespritzt worden ist bzw. ob
es sich um ein Bio-Produkt handelt, sehe ich davon ab.

Als [Nachmittags-Workout][2] habe ich mir etwas für den Oberkörper ausgesucht.
So langsam vermisse ich meine Klimmzugstange. Wenn ich nur früher schon gewusst
hätte, was man damit alles - abgesehen von Klimmzügen - machen kann.

Am Nachmittag tauche ich weiter in die Dokumentation von Hugo ein und verbringe
damit ein paar Stunden. Das anschließende Abendessen ist in Ordnung, den
Couscous-Salat habe ich allerdings schon mal gesehen - das ist kein Problem,
er schmeckt hervorragend (und ist leicht pikant).

Morgen kann ich mir zur Abwechslung den Gottesdienst-Stream ansehen. Ein wenig
"Normalität", auch wenn der Stream auch nur aufgrund der Pandemie existiert.
Verrückt.

## Playlist

+ Alient Ant Farm: TrueANT
+ Meat Loaf: Hits out of Hell

[img1]: {static}/images/2020-06-27_mahlzeit_1.jpg
[1]: https://darebee.com/workouts/busy-bee-workout.html
[2]: https://darebee.com/workouts/total-upperbody-workout.html

