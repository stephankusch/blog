Title: Tag 4: Tieftauchgang
Date: 2020-06-26T20:43+02:00
Category: Quarantäne
Tags: 
Slug: tag-4
Authors: Stephan Kusch
Status: published

Es geschehen noch Zeichen und Wunder: Käse zum Frühstück! Nicht, dass ich ihn
dort unbedingt bräuchte, einer große Schüssel Müsli mit Hafermilch gebe ich
immer den Vorzug, das ist von einem Viersternehotel aber vielleicht zu viel
(oder zu wenig?) verlangt. Gleichzeitig werde ich allerdings nicht zu den Leuten
zählen, die die Küche mit Sonderwünschen überfallen.

Bevor es zum Hofgang geht, gibt es wieder [ein Cardio-Workout][1]. Macht Spaß, ist
aber ein bisschen zu einfach. Gestern war härter.

Nach dem [Mittagessen][img1] (Fleisch...) folgt ein Workout aus der Kategorie
"anstrengend". Die seit Tagen steigende Temperatur im Zimmer trägt zu einem
erheblichen Schweißtrieb bei. Zeit für die Dusche. Danach gibt es wieder Kaffee.
Es zeichnet sich ein täglich wiederkehrendes Muster ab. Das ist gut.

Am Nachmittag mache ich einen tiefen Tauchgang in die Welt der _Static Site_
_Generators_, nachdem ich vom minimalen CMS [Pico][3] bzw. dessen
Implementierung als Nextcloud-Plugin nicht überzeugt bin. Zu viele Probleme, zu
unflexibel. Außerdem müsste ich mangels Angeboten eigens dafür ein Theme
erstellen. Ziel der Übung ist, Joomla als CMS der Männerchor-Website abzulösen,
da es unnötige Komplexität mitbringt, schwierig zu warten ist (danke, lieber
Webmaster!), vergleichsweise langsam arbeitet (dank Shared Hosting) und
natürlich einen Angriffsvektor darstellt. Ich bleibe bei [Hugo][4] und hängen.
Bisher habe ich nur Erfahrung mit Pelican, aber Hugo scheint aus Anwendersicht
ähnlich zu funktionieren; außerdem ist die Auswahl an hochwertigen Themes enorm.

## Playlist

+ Freundeskreis: Esperanto

[img1]: {static}/images/2020-06-26_mahlzeit_1.jpg
[1]: https://darebee.com/workouts/all-out-workout.html
[2]: https://darebee.com/workouts/imperium-workout.html
[3]: http://picocms.org
[4]: https://gohugo.io

