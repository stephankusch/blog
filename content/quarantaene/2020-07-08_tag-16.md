Title: Tag 16: Bockig
Date: 2020-07-08T19:46+02:00
Category: Quarantäne
Tags: 
Slug: tag-16
Authors: Stephan Kusch
Status: published

Ich habe den Verdacht, dass die Bewegungsarmut und reichhaltige Verpflegung auf
meine Leibesfülle schlagen. Deshalb verordne ich mir ab sofort härtere Workouts
mit einem Anteil Burpees. Der Burpee ist die ultimative Übung, da man den
Körperschwerpunkt innnerhalb kürzester Zeit um eine große Höhendifferenz gegen
die Schwerkraft bewegen muss. Physikalisch betrachtet ist dafür eine je nach
Größe und Körpermasse große Leistung notwendig, der Körper wird auf Hochtouren
gebracht. Rechnerisch dürfte nur Hochsprung noch mehr Leistung verlangen;
allerdings kann man Burpees mit großer Frequenz ausführen und damit die nötige
Arbeit (Energie = Leistung * Zeit) beliebig steigern. Kurz: es ist extrem
anstrengend. ;)

+ [Warmup][1]
+ [Workout][2]
+ [Stretching][3]

Da das [Mittagessen][img1] wieder einmal 20 min vor der Tür stand, ist es
verständlicherweise kalt. Interessanterweise scheint sich die Menüfolge zu
wiederholen, denn genau dieses Gulasch und Gemüse habe ich hier schon mal
gesehen, allerdings mit einer anderen Beilage.

+ [Warmup][4]
+ [Workout][5]
+ [Stretching][6]

Heute klappt es wieder mit dem Kaffee, da keine ungebetene Veranstaltung
dazwischen kommt.

Zum Abendbrot gibt es wieder den bekannt-beliebten Couscous-Salat und etwas
Rohkost mit Dipp. Vielleicht versucht man, uns jetzt über das Essen versöhnlich
zu stimmen. _Der_ Drops ist für meine Zimmernachbarn jedenfalls gelutscht: die
rühren das Essen nur noch sporadisch an und lassen sich Alternativen bringen.
Ich würde das als _bockig_ bezeichnen, kann es allerdings verstehen. Ich selbst
bin nur einfach nicht der Typ, der bockig wird.

[img1]: {static}/images/2020-07-08_mahlzeit_1.jpg
[1]: https://darebee.com/workouts/quick-warmup-workout.html
[2]: https://darebee.com/workouts/overkill-workout.html
[3]: https://darebee.com/workouts/morning-stretch-workout.html
[4]: https://darebee.com/workouts/classic-warmup-workout.html
[5]: https://darebee.com/workouts/concrete-core-workout.html
[6]: https://darebee.com/workouts/rest-and-rec-workout.html

