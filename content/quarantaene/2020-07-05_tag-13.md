Title: Tag 13: Die Spannung steigt
Date: 2020-07-05T20:13+02:00
Category: Quarantäne
Tags: 
Slug: tag-13
Authors: Stephan Kusch
Status: published

Ein Tag ist wie der andere. Meine Fähigkeit zur Selbstmotivation wird auf eine
harte Probe gestellt. Dazu kommen meine überlasteten Mittelfußknochen, die mich
davon abhalten, laufen zu gehen. Das scheint sowieso nicht mehr möglich zu sein,
weil die Bewegungsfreiheit im Hofgang laut Zimmernachbar erheblich eingeschränkt
worden ist. Da wird es selbst für native Stubenhocker wie mich schwierig, bei
Laune zu bleiben.

+ [Warmup][1]
+ [Workout][2]
+ [Stretching][3]

Den Hofgang verbringe ich sitzend, Kaffee trinkend, mich unterhaltend. Ab morgen
werden meine Klamotten verpackt sein, so dass ich sowieso keinen Sport mehr
werde treiben können. Das Nachmittags-Workout wird also (hoffentlich) meine
lezte Tätigkeit iin diesem Hotel sein. Zunächst aber zum Mittagessen.

Im Vergleich ist es im Mittelfeld. Lauwarm, aber auf der Habenseite sind das gut
abgeschmeckte Gemüse, die Folienkartoffel mit Butter, Salz (!) und Kümmel. Das
Hühnerbein ist eine willkommen Abwechslung.

Das Nachmittagsworkout ist tatsächlich schweißtreibend:

+ [Warmup][4]
+ [Workout][5]
+ [Stretching][6]

Insebesondere die Liegestütze "mit Klatschen" wecken Erinnerungen an vergangene
Zeiten und Unbehagen in den Handgelenken.

Heute gebe ich das Gepäck ab. Es hat alles gepasst - die Details bezüglich
Flüssigkeiten und spitzer und scharfer Gegenstände (Nagelschere...) müssen
natürlich beachtet werden. Einer der Gründe, warum ich lieber mit dem Zug
verreise.

Ich freue mich jedenfalls auf den heutigen Münster-Tatort. Da gibt es was zu
lachen.

## Podcast

+ [Tagesschau: mal angenommen][7]

## Playlist

+ The Kooks: Inside In/Inside Out

## Artikel des Tages

+ https://netzpolitik.org/2020/kritischer-bericht-spionierende-smart-tvs-in-jedem-wohnzimmer/

[img1]: {static}/images/2020-07-05_mahlzeit_1.jpg
[1]: https://darebee.com/workouts/quick-warmup-workout.html
[2]: https://darebee.com/workouts/size-down-workout.html
[3]: https://darebee.com/workouts/sore-muscles-stretch-workout.html
[4]: https://darebee.com/workouts/classic-warmup-workout.html
[5]: https://darebee.com/workouts/popeye-workout.html
[6]: https://darebee.com/workouts/upperbody-stretch-workout.html
[7]: https://www.tagesschau.de/multimedia/podcasts/malangenommen-ausserirdische-101.html

