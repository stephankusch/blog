Title: Impressum
Date: 2016-09-22T17:50+02:00
Slug: impressum

## Kontakt

Stephan Kusch

E-Mail: stephan.kusch@posteo.de  
PGP: 9754 CB67 93BB EA71 6617 477A 731D E5C0 FBA2 EA34

XMPP: stephan.kusch@ffnord.net

## Lizenz dieser Seite

Die Inhalte dieser Seite sind lizenziert unter einer
[Creative Commons Namensnennung 4.0 International Lizenz](https://creativecommons.org/licenses/by/4.0/).

## Blog-Software

Diese Seiten werden aus

* [Markdown](https://daringfireball.net/projects/markdown)-Dateien mit
* [Pelican](https://blog.getpelican.com), einem Generator für statische Webseiten
unter Verwendung des
* [Alchemy-Themes](https://github.com/nairobilug/pelican-alchemy) erstellt und von
* [GitLab.com](https://gitlab.com) gehostet.
