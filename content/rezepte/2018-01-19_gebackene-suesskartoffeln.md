Title: Gebackene Süßkartoffeln mit Avocado-Paprika-Creme
Date: 2018-01-19T16:48+01:00
Category: Rezepte
Tags: Vegetarisch
Slug: gebackene-suesskartoffeln
Authors: Stephan Kusch
Status: published

## Zutaten

Für zwei Personen:

* 2 Süßkartoffeln zu je etwa 300 g
* 2 Avocados
* 2 Paprikaschoten
* 150 g Frischkäse gewünschter Fettstufe
* 1 kleine Zwiebel oder Schalotte
* 1 Knoblauchzehe
* etwas Limettensaft
* Petersilie
* Sonnenblumenöl
* Pfeffer
* Salz
* Chili- oder Paprikagewürz


## Vorbereitung

1. Wasche die Süßkartoffeln, Paprika und Petersilie gründlich.
2. Ziehe Knoblauch und Zwiebeln ab.

## Zubereitung

1. Pinsele die Süßkartoffeln mit dem Öl ein und gare sie im Backofen auf einem
   Backblech mit Backpapier für etwa eine Stunde bei ca. 200 °C.
2. Vermenge das Fruchtfleisch der Avocados mit dem Frischkäse, füge die fein
   gehackte Zwiebel und Knoblauch hinzu.
3. Würfele die Paprika, hacke die Petersilie und füge jeweils etwa ein
   Dreiviertel der Menge der Creme hinzu.
4. Schmecke die Creme mit Limettensaft, Chili bzw. Paprika, Pfeffer und Salz ab.
5. Richte die garen Süßkartoffeln längs eingeschnitten mit der Creme als Füllung
   an und bestreue sie mit den übrigen Paprikawürfeln und der übrigen
   Petersilie.

## Nährwert

Pro Portion etwa 500 kcal bei Verwendung von entrahmtem Frischkäse.

## Quelle

Gefunden auf [Chefkoch.de](https://www.chefkoch.de/rezepte/2725361425059942/Gebackene-Suesskartoffeln-mit-Avocado-Paprika-Creme.html?portionen=2)
und nach eigenem Gusto angepasst.
