Title: Krautsalat nach Südtiroler Art
Date: 2017-09-02T17:41+02:00
Category: Rezepte
Tags: Beilage
Slug: krautsalat
Authors: Stephan Kusch
Status: published

## Zutaten

Für zwei Personen:

* Ein halber Weißkohlkopf
* 100 g Bauchspeck, luftgetrocknet
* 100 mL Weißweinessig
* Salz
* Pfeffer
* Kümmel
* Olivenöl


## Vorbereitung

1. Schneide den Kohl klein, gebe ihn in eine ausreichend große Schüssel und bestreue ihn mit Salz. Lass ihn eine Stunde ziehen und rühre ihn in der Zwischenzeit gelegentlich durch.
2. Gib den entwässerten Kohl in einen Durchschlag oder Sieb und spüle ihn unter Rühren mit Wasser so lange ab, bis der Salzgehalt ein erträgliches Maß erreicht hat. Gib ihn danach in eine Schüssel.
3. Schneide den Bauchspeck klein.

## Zubereitung

1. Lasse den Bauchspeck in einem kleinen Topf bei mittlerer Temperatur bis zum gewünschten Bräunungsgrad aus.
2. Lösche den Bauchspeck mit dem Weißweinessig ab und gib das Gemisch vollständig über den Kohl.
3. Rühre alles kräftig durch, gib einen Esslöffel Olivenöl hinzu und würze mit Pfeffer und Kümmel.

## Nährwert

Eher gering. Knödel empfehlen sich als reichhaltige Beilage.
