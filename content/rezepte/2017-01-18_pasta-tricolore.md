Title: Pasta Tricolore
Date: 2017-01-18T19:49+01:00
Category: Rezepte
Tags: Pasta, Vegetarisch
Slug: pasta-tricolore
Authors: Stephan Kusch
Status: published

## Zutaten

Für zwei Personen:

* 250 g Tagliatelle
* 160 g Quark
* 120 g Kirschtomaten
* 4 TL Kapern
* 2 EL Olivenöl
* 1 Zwiebel
* Basilikum
* Pfeffer
* Salz
* Chilipulver

## Vorbereitung

1. Schneide die Zwiebel in kleine Stücke und
2. hacke die Kapern.

## Zubereitung

1. Setze einen großen Topf mit reichlich Wasser für die Nudeln auf. Gebe
großzügig Salz dazu, sobald das Wasser kocht; lass die Nudeln etwa fünf bis
sieben Minuten darin garen, bis sie al dente sind.

2. Erhitze in der Zwischenzeit das Olivenöl in einer großen Pfanne und lasse die
Zwiebelstückchen darin dünsten, bis sie glasig sind.

3. Füge Kapern und Tomaten hinzu, setze einen Deckel auf die Pfanne, reduziere
die Hitze und lasse den Inhalt für etwa fünf bis sieben Minuten schmoren.

4. Gieße die gegarten Nudeln ab und gib sie in die Pfanne. Durchmenge alles
vorsichtig.

5. Füge den Quark hinzu und würze nach belieben mit Pfeffer, Salz, Basilikum und
Chilipulver. Durchmenge alles vorsichtig.

6. Richte das Gericht in tiefen Tellern an.

## Nährwert

Pro Portion etwa 340 kcal bzw. 1424 kJ. Es bleibt Raum für Nachtisch.
