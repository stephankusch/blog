Title: Cheddar-Ale-Soup
Date: 2017-01-18T20:19+01:00
Category: Rezepte
Tags: Bier
Slug: cheddar-ale-soup
Authors: Stephan Kusch
Status: published

## Zutaten

Für zwei Personen:

* 6 Scheiben Frühstücksspeck
* 1 Zwiebel
* 1 Paprika
* 2 Karotten
* 3 Knoblauchzehen
* 10 g Butter
* 500 mL Ale
* 500 mL Milch
* 250 mL Gemüsebrühe
* 250 g Cheddar
* 80 g Mehl
* Senf
* Worcestershire-Sauce
* Cayenne-Pfeffer
* Pfeffer
* Salz

## Vorbereitung

1. Schneide Zwiebel, Paprika, Karotten und Knoblauch in mundgerechte Stücke.
2. Reibe den Cheddar.
3. Brate den Frühstücksspeck bei möglichst niedriger Temperatur aus (um die
Bildung von Nitrosaminen zu verringern).

## Zubereitung

1. Gib die Butter zum Bratensatz des Specks und lass sie zerlaufen.
2. Füge Zwiebeln, Paprika und Karotten hinzu und lasse alles bei geschlossenem
Deckel für 15 min garen. Füge anschließend den Knoblauch hinzu.
3. Verteile das Mehl gleichmäßig auf dem Gargut und lass es unter ständigem
Rühren eindicken. Vermeide, dass es anbrennt.
4. Gib unter Rühren das Ale dazu und lass es für etwa 5 min köcheln.
5. Würze nach Belieben mit Senf, Worcestershire-Sauce und Cayenne-Pfeffer und
rühre anschließend die Milch ein.
6. Lass die Suppe kurz aufkochen. Reduziere die Hitze und rühre den Cheddar
portionsweise ein. Dabei darf die Suppe nicht wieder aufkochen.
7. Schmecke die Suppe mit Salz und Pfeffer ab, sobald sämtlicher Käse
geschmolzen ist.
8. Garniere die angerichtete Suppe mit dem zuvor ausgebratenem Speck.

## Nährwert

Reichhaltig.
