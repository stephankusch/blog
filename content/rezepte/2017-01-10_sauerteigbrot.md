Title: Sauerteigbrot
Date: 2017-01-10T18:04+01:00
Category: Rezepte
Tags: Backen, Vegetarisch
Slug: sauerteigbrot
Authors: Stephan Kusch
Status: published

## Vorbemerkungen

Der Einfachheit wegen verwende ich Roggenmehl und Weizenmehl. Beide können
beliebig durch Vollkornvarianten ersetzt werden. Außerdem kann statt Weizen
problemlos in variablem Anteil auch Dinkel, Einkorn oder Emmer verwendet werden.

In diesem Rezept verwende ich außerdem einen ganzen Würfel Hefe zum Backen. Mit
zunehmendem Alter des Sauerteigs steigt zumeist auch seine Triebkraft, so dass
die Menge an zugesetzter Hefe reduziert werden kann. Einige Sauerteige, gerade
reine Weizensauerteige, können so triebstark sein, dass ganz auf Hefe verzichtet
werden kann.

## Einen Sauerteig ansetzen

Vermenge in einem Schraubdeckelglas etwa fünf Teelöffel Roggenmehl mit genau so
viel Wasser, dass ein flüssiger Brei entsteht. Setze den Deckel lose auf, so
dass ein Luftaustausch stattfinden, aber nichts hereinrieseln kann und verwahre
das Glas bei Raumtemperatur. Füge einmal täglich einen Teelöffel Roggenmehl und
so viel Wasser hinzu, dass der Brei die ursprüngliche Konsistenz behält.
Innerhalb von drei Tagen sollte sich ein charakteristischer Geruch nach faulem
Obst, Alkohol und/oder Erbrochenem entwickeln. Wenn der Teig nach fünf Tagen
nicht verschimmelt ist, sollte er stabil sein und kann zum Backen verwendet
werden.

## Den Teig führen

Vermenge in einer Rührschüssel 300 g Roggenvollkornmehl und füge so viel Wasser
hinzu, dass ein zäher Teig entsteht. Gib den Sauerteig aus dem Glas vollständig
dazu und durchmische alles kräftig. Lass den Teig 24 h bei Raumtemperatur
abgedeckt stehen. Entnimm dem Teig zwei bis drei Esslöffel und bewahre ihn in
einem verschlossenen Schraubdeckelglas auf. Im Kühlschrank beträgt die
Haltbarkeit höchstens zwei Wochen. Eingefroren hält er sich deutlich länger.

## Das Brot backen

Gib 300 g Weizenmehl, einen Würfel Hefe und 20 g Salz zum Teig und vermenge
alles mit einem Handrührgerät oder den Händen. Füge dabei so viel Wasser
hinzu, dass ein knetbarer, aber immer noch klebriger, weicher Teig entsteht.
Lass diesen Teig etwa eine Stunde gehen.

Knete den Teig nach dem Gehen nochmal und überführe ihn mit einem Teigschaber
aus der Rührschüssel auf die mit reichlich Mehl bestreute Arbeitsfläche. Forme
den Teig zu einer dicken Wurst, dabei ist es hilfreich, wenn Hände und Teig mit
so viel Mehl bedeckt sind, dass der Teigling nicht an den Händen kleben bleibt
und aufreißt. Lege die Teigwurst in eine Kastenform, drücke den Teig
formschlüssig fest und decke ihn mit einem feuchten Tuch ab.

Heize den Ofen auf 200 °C vor. Eine mit Wasser gefüllte Schale im Ofen sorgt für
eine hohe Luftfeuchtigkeit und verbessert das Backergebnis. Der Teigling ist zum
Backen fertig, wenn er sich unter dem feuchten Tuch wölbt. Schneide ihn mit
einem Messer einige Male ein und backe ihn etwa 50 min.

Stürze das fertige Brot direkt nach dem Backen aus der Form und lass es, in ein
Geschirrtuch eingewickelt, auf einem Rost abkühlen. Nach frühestens zwei Stunden
ist es schnittfähig.
