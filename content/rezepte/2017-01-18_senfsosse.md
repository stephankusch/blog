Title: Senfsoße
Date: 2017-01-18T20:04+01:00
Category: Rezepte
Tags: Vegetarisch
Slug: senfsosse
Authors: Stephan Kusch
Status: published

## Zutaten

Für zwei Personen:

* 100 g Senf
* 20 g Maisstärke
* 500 mL Gemüsebrühe


## Vorbereitung

1. Setze die Gemüsebrühe rechtzeitig an.
2. Schlämme die Maisstärke in wenig Wasser oder Brühe auf.

## Zubereitung

1. Erwärme die Gemüsebrühe und rühre währenddessen mit einem Schneebesen den
Senf unter.
2. Rühre die aufgeschlemmte Stärke in kleinen Portionen unter die Senfbrühe und
halte sie etwa eine Minute am gelinden Sieden.

## Nährwert

Insgesamt etwa 160 kcal bzw. 682 kJ.
