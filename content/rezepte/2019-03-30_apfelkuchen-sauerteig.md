Title: Apfel-Streuselkuchen mit Sauerteig
Date: 2019-03-30T12:41+01:00
Category: Rezepte
Tags: Backen, Vegan
Slug: apfelkuchen-sauerteig
Authors: Stephan Kusch
Status: published

## Zutaten für ein Blech

Boden:

+ Anstellgut für einen Sauerteig
+ 500 g Mehl (z. B. Dinkelvollkorn oder Type 1050)
+ 50 g temperaturbeständiges Pflanzenöl
+ 60 g Zucker
+ 1 TL Vanillezucker
+ Wasser auf Zimmertemperatur

Belag:

+ 7-8 Äpfel (z. B. Boskoop)
+ 50 g Zucker
+ Saft einer Zitrone

Streusel:

+ 200 g Mehl (z. B. Dinkelmehl Type 1050)
+ 200 g Zucker
+ 50 g temperaturbeständiges Pflanzenöl
+ 1 TL Vanillezucker
+ Zimt

## Vorbereitung

Für den Sauerteig vermengst du 250 g bis 300 g Mehl mit so viel zimmerwarmem
Wasser, dass ein zähflüssiger Teig entsteht. Dazu gibst du das Anstellgut,
rührst es unter und deckst den Teig ab. Er muss nun 24 h bei Zimmertemperatur
ruhen (streng genommen arbeitet er).

## Zubereitung

Die Äpfel schneidest du - gern mit Schale - in Würfel, vermengst sie mit Zucker
und Zitronensaft und köchelst auf kleiner Flamme in etwa 20 min ein Kompott
daraus.

Für die Streusel vermengst du alle Zutaten und knetest so lange mit der Hand,
dass ein homogener, bröckeliger Mürbeteig entsteht. Die Konsistenz ist mit einem
butter- und eihaltigen Teig nicht vergleichbar, die Streusel sind instabil.

Zum Sauerteig gibst du das übrige Mehl und die anderen Zutaten und knetest etwa
zehn Minuten z. B. mit dem Handrührgerät. Bei Bedarf gibst du so viel Wasser
dazu, dass ein geschmeidiger, fester Teig entsteht. Er wird wahrscheinlich recht
klebrig sein. Lass ihn etwa eine Stunde ruhen. Danach verteilst du ihn mit einem
Teigschaber möglichst gleichmäßig auf einem gefetteten oder mit Backpapier
ausgelegten Blech und lässt ihn noch etwa eine bis maximal zwei Stunden gehen.

Verteile das Apfelkompott gleichmäßig auf dem Teig. Streue nun die Streusel
darüber. Dazu empfiehlt es sich, den Streuselteig fest in der Hand zu verdichten
und einzelne Streusel daraus direkt auf den Kuchen zu bröseln.

Backe den Kuchen anschließend etwa 30 min bei 160 °C.

## Nährwert

Noch nicht berechnet.

## Quellen

+ Hefeteig nach [Dr. Oetker](https://www.oetker.de/rezepte/r/grundrezept-hefeteig-mit-frischer-hefe.html)
+ Streusel nach [Dr. Oetker](https://www.oetker.de/rezepte/r/apfel-streusel-kuchen-aus-der-springform.html)
