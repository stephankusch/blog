Title: Zehn einfache Sicherheitsmaßnahmen
Date: 2016-10-08T16:22+02:00
Category: Anleitungen
Tags: Linux, Sicherheit
Slug: zehn-einfache-sicherheitsmassnahmen
Authors: Stephan Kusch
Status: published

Dieser Artikel ist eine freie Übersetzung eines [Blog-Posts von Kevin Fenzi][1].

So, wie man zu Hause Türen und Fenster schließt, gibt es auch am eigenen Rechner
Tätigkeiten, die jeder beherzigen sollte.

1. Wähle lange Passphrasen (bzw. _Mantras_), an die du dich erinnern kannst.
Damit ist eine Folge aus mehreren verschiedenen Wörtern gemeint.
2. Verschlüssele deine Massenspeicher (Festplatten, SSDs...) während der
Installation des Betriebssystems. Der Geschwindigkeitsverlust ist unmerklich.
Sollte dein Massenspeicher abhanden kommen, z. B. nach Entsorgung oder
Diebstahl, sind die Daten für Dritte unleserlich.
3. Installiere regelmäßig Updates. Bist du eher vergesslich, dann setze einen
Automatismus auf, der dir die Arbeit täglich oder über Nacht abnimmt.
4. Starte den Rechner neu, wenn es ein Kernel- oder glibc-Update gab. Gewöhne
dich daran, regelmäßig neuzustarten.
5. Sperre den Rechner beim Verlassen, wenn du dich unter Leuten befindest, denen
du nicht vertraust.
6. Lege Backups an und prüfe diese regelmäßig. Backups mögen nicht als
Sicherheitsmaßnahme erscheinen, sind es aber tatsächlich. Beispielsweise könnte
sogenannte Ransomware deine Daten verschlüsseln - mit einem aktuellen Backup
kannst du deine Festplatten leeren, ein neues System aufsetzen und das Backup
einspielen.
7. Öffne keine merkwürdigen Anhänge oder Links in E-Mails von unbekannten
Absendern.
8. Stecke keine unbekannten Geräte in deine USB-Buchsen oder ähnliche. Es gibt
ein nützliches Werkzeug, _usbguard_, das verhindert, dass dies in deiner
Abwesenheit andere tun.
9. Nutze einen Passwort-Manager oder ein System, das es dir erlaubt, lange,
nicht leicht zu erratende Passwörter oder Mantras zu nutzen. Passwort-Manager
sind zum Beispiel *KeePassX* oder *pass*, ein System ist beispielsweise
[Diceware][2].
10. Wenn du ein Laptop nutzt und/oder viel reist, kann es sinnvoll sein, deinen
Datenverkehr durch ein VPN zu leiten. Solange du über einen Endpunkt verfügst,
kannst du nahezu all deinen Datenverkehr darüberschicken und Probleme mit
lokalen Datenschnüfflern vermeiden.

Einige dieser Dinge erfordern eine einmalige Investition von Zeit, andere
erfordern das Einüben von Gewohnheiten, alle sind den Aufwand wert.

Werden diese Maßnahmen den Rechner _sicher_ machen? Sicherlich nicht. Absolute
Sicherheit gibt es nicht, sie ist ein Prozess der Gefährdungsbeurteilung und der
(nicht) erforderlichen Maßnahmen. Die oben beschriebenen Maßnahmen schützen
recht ordentlich vor manchen Bedrohungen (leicht zu erratende Passwörter,
Unbefugte am Rechner, Abhören des Datenverkehrs, bekannte Sicherheitslücken in
Software und andere), versagen aber bei anderen (Keylogger, Aufforderung zur
Herausgabe unter Androhung von Gewalt, Überwachungskameras, Zero-Day-Exploits,
*Social Engineering*).

[1]: https://www.scrye.com/wordpress/nirik/2016/10/05/10-basic-linux-security-measures-everyone-should-be-doing/
     "10 basic linux security measures everyone should be doing"
[2]: https://de.wikipedia.org/wiki/Diceware "Diceware"
