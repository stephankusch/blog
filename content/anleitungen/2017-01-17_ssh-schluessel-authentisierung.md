Title: SSH-Zugriff mit einer Schlüsseldatei authentisieren
Date: 2017-01-17T20:22+01:00
Category: Anleitungen
Tags: Linux, Kryptographie
Slug: ssh-schluessel-authentisierung
Authors: Stephan Kusch
Status: published

In dieser Anleitung wird eine Authentisierung mit dem Signaturverfahren Ed25519
verwendet, weil es schnell und sicher ist. Allerdings wird es von SSH erst ab
Version 6.5 unterstützt. Zum Zeitpunkt der Veröffentlichung dieses Textes lag
SSH auf Debian 8 in Version 6.7 vor. Die Anleitung ist dem [Arch Wiki][1] aus
dem Englischen entnommen.

Erstelle eine Schlüsseldatei:

	:::bash
	ssh-keygen -t ed25519

Das Schlüsselpaar wird standardmäßig im Verzeichnis `~/.ssh` abgelegt. Die Wahl
des Passworts sollte davon abhängen, in welcher Umgebung die Datei gespeichert
wird. Auf einer verschlüsselten Festplatte, zu der nur du selbst Zugang hast,
kann ein kurzes oder gar kein Passwort genügen. Könnte die Datei durch einen
Systemadministrator oder anderen Zugriff kompromittiert werden, empfiehlt sich
ein starkes [Mantra](https://de.wikipedia.org/wiki/Diceware).

Wenn du das Passwort nachträglich ändern möchtest, kannst du das mit dem Befehl

	:::bash
	ssh-keygen -f ~/.ssh/id_ed25519 -p

erledigen.

Kopiere nun den öffentlichen Teil des Schlüsselpaars auf die entfernte Maschine:

	:::bash
	ssh-copy-id -i ~/.ssh/id_ed25519.pub benutzername@domain.tld

Melde dich auf dem entfernten System an:

	:::bash
	ssh benutzername@domain.tld

Um eine Anmeldung mittels Passwort zu unterbinden, setze in der Datei
`/etc/ssh/sshd_config` die Option

	:::text
	# Authentication
	PermitRootLogin without-password

und starte `sshd` neu:

	:::bash
	sudo systemctl restart sshd

[1]: https://wiki.archlinux.org/index.php/SSH_keys "SSH keys"
