Title: Linux auf einem Dell Inspiron Mini 1012 verwenden
Date: 2017-01-01T16:30+01:00
Modified: 2017-02-20T17:53+01:00
Category: Anleitungen
Tags: Linux, Fedora, Ubuntu, Dell
Slug: dell-inspiron-mini-1012-linux
Authors: Stephan Kusch
Status: published

Der Betrieb eines Dell Inspiron Mini 1012 kann ab Linux-Kernelversion 4.1 bei
gleichzeitiger Verwendung von systemd schwierig sein: Nach dem ersten Neustart
bootet das System nicht mehr vollständig. Dafür verantwortlich ist eine Datei,
die beim Herunterfahren angelegt wird, und den Zustand der
Tastaturhintergrundbeleuchtung wiederherstellbar machen soll - das Gerät hat
eine solche allerdings nicht. Derzeit sieht es nicht danach aus, dass dieser
Fehler im Kernel je behoben werden wird. Das Problem umgeht man unter Fedora,
indem man systemd anweist, nicht zu versuchen, den Zustand wiederherzustellen.
Diesen Kniff habe ich dem [RedHat-Bugzilla][1] entnommen.

Dazu drückt man in Grub die Taste `e` und fügt der Zeile, die die Boot-Parameter
enthält, den Parameter

`systemd.restore_state=0`

hinzu. Mit `STRG+x` bootet das System dann wie gewünscht. Um die Änderung
permanent zu machen, editiert man die Grub-Default-Konfigurationsdatei:

	:::bash
	sudo nano /etc/default/grub

und fügt dort den oben genannten Parameter dem Feld `GRUB_CMDLINE_LINUX` hinzu.
Nun lässt man die Grub-Konfigurationsdatei neu erstellen:

	:::bash
	sudo grub2-mkconfig -o /boot/grub2/grub.cfg

Das Betriebssystem sollte nun problemlos starten.

Unter Ubuntu Mate 16.04 lässt sich das Problem auf andere Weise lösen. Das
Kernel-Modul `dell_laptop` muss dazu deaktiviert werden. Dadurch ergeben sich
meiner Beobachtung nach aber keine weiteren Nachteile im Betrieb des Laptops.
Den Hinweis dazu habe ich im [Bugtracker von Canonicals Launchpad][2] gefunden.

Erstelle im Verzeichnis `/etc/modprobe.d` eine Datei, zum Beispiel

	:::bash
	sudo nano /etc/modprobe.d/blacklist-dell.conf

und füge dort den den Text

	:::text
	blacklist dell_laptop

ein. Regeneriere das `initramfs` mit

	:::bash
	sudo update-initramfs -u

und starte den Rechner neu.

Offenbar ist das Problem stark von der verwendeten Desktop-Umgebung abhängig,
Mate löst den Bug aus, XFCE tut es nicht.

[1]: https://bugzilla.redhat.com/show_bug.cgi?id=1253523
     "Boot Hang (Apparent systemd Problem) on Dell Inspiron 1012"
[2]: https://bugs.launchpad.net/dell-sputnik/+bug/1510344/comments/9
     "Bugs: Dell Sputnik"
