Title: WLAN auf einem Lenovo IdeaPad V510 verwenden
Date: 2017-09-09T19:41+02:00
Modified: 2017-09-09T19:41+02:00
Category: Anleitungen
Tags: Linux, Fedora, Lenovo, WLAN
Slug: lenovo-ideapad-v510-linux
Authors: Stephan Kusch
Status: published

Auf meinem Lenovo IdeaPad V510-15IKB lässt sich das WLAN-Interface der
integrierten Intel Wireless 3165 nicht aktivieren. Das liegt daran, dass es vom
Kernelmodul _ideapad\_laptop_ blockiert wird. Dieses Verhalten scheint erratisch
zu sein, ich weiß bisher aber nicht, ob es behoben werden soll. Die Lösung habe
ich nach kurzer Suche in [Ask Ubuntu][1] gefunden.

Um das WLAN-Interface trotzdem nutzen zu können, muss das schuldige Modul
deaktivert werden. Das geschieht so:

	:::bash
	sudo modprobe -r ideapad_laptop

Danach wird rfkill angewiesen, den Block des Interfaces aufzuheben.

	:::bash
	sudo rfkill unblock all

Um den Eingriff permanent zu machen, erstelle im Verzeichnis `/etc/modprobe.d`
eine Datei, zum Beispiel

	:::bash
	sudo nano /etc/modprobe.d/blacklist-ideapad.conf

und füge dort den den Text

	:::text
	blacklist ideapad_laptop

ein. Regeneriere das `initramfs` mit

	:::bash
	sudo dracut --regenerate-all --force

[1]: https://askubuntu.com/questions/737803/wifi-intel-ac-3165-yoga-700-14isk-does-not-work-with-ubuntu
     "Wifi Intel AC 3165 Yoga 700-14ISK, does not work with ubuntu"
