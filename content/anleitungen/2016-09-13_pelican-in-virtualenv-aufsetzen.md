Title: Pelican in einer virtualisierten Umgebung aufsetzen
Date: 2016-09-13T19:34+02:00
Modified: 2019-03-31T15:47+02:00
Category: Anleitungen
Tags: Python
Slug: pelican-in-virtualenv-aufsetzen
Authors: Stephan Kusch
Status: published

Die Informationen für diese Anleitung habe ich den Dokumentationen von
[virtualenv][1] und [Pelican][2] entnommen.

Installiere `virtualenv`:

	:::bash
	sudo dnf install python3-virtualenv

Navigiere zum Pfad des Projekts:

	:::bash
	cd ~/name/projekt

Dann erstelle das virtualenv:

	:::bash
	python3 -m venv env

Aktiviere das virtualenv:

	:::bash
	source env/bin/activate

Der Prompt zeigt nun das aktuelle virtualenv an. Um es zu verlassen, genügt es,

	:::bash
	deactivate

einzugeben.

Im virtualenv sind einige Programme vorinstalliert. Mittels `pip` wird dort
_Pelican_ installiert:

	:::bash
	pip install pelican

Zur Verwendung von _Markdown_ muss das entsprechende Paket installiert werden:

	:::bash
	pip install Markdown

Um ein identisch eingerichtetes virtualenv an einem anderen Ort einzurichten,
lässt du pip eine Liste mit den installierten Paketen und deren Version
erstellen:

	:::bash
	pip freeze > requirements

Die Datei `requirements` lässt sich in einem anderen virtualenv wieder einlesen:

	:::bash
	pip install -r requirements

Updates für diese Pakete installierst du so:

	:::bash
	pip install --upgrade -r requirements

[1]: https://virtualenv.pypa.io/en/stable/userguide/ "virtualenv documentation"
[2]: http://docs.getpelican.com/en/3.6.3/install.html "Installing Pelican"
