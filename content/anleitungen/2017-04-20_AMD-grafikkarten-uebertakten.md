Title: AMD-Grafikkarten unter AMDGPU übertakten
Date: 2017-04-20T22:56+02:00
Category: Anleitungen
Tags: Linux
Slug: AMD-grafikkarten-uebertakten
Authors: Stephan Kusch
Status: draft

Füge `amdgpu.ppfeaturemask=0x7fff` der Kernel-Bootzeile hinzu.


	:::bash
	echo 5 > /sys/class/drm/card0/device/pp_sclk_od
