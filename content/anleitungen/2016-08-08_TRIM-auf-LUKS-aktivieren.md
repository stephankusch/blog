Title: TRIM auf LUKS-verschlüsselten Datenträgern aktivieren
Date: 2016-08-08T18:00+02:00
Modified: 2017-09-11T22:34+02:00
Category: Anleitungen
Tags: Fedora, Kryptographie
Slug: trim-auf-luks-aktivieren
Authors: Stephan Kusch
Status: published

> ## Update
> 
> Seit Fedora 26 wird die Option `discard` bei Neuinstallation der Distribution
> automatisch in die crypttab geschrieben, so dass der Befehl `fstrim`
> grundsätzlich funktioniert.

## Ursprünglicher Artikel

Diese Anleitung stammt aus [Christopher Smart's Blog][1].Getestet habe ich sie
unter Fedora 24 und 25.

Öffne die Datei `/etc/default/grub`:

	:::bash
	sudo nano /etc/default/grub

Füge der Zeile `GRUB_CMDLINE_LINUX` das Argument `rd.luks.options=discard` an
und speichere die Datei.

Erstelle die Grub-Konfiguration neu:

	:::bash
	sudo grub2-mkconfig -o /boot/grub2/grub.cfg

Öffne die Datei `/etc/crypttab`:

	:::bash
	sudo nano /etc/crypttab

Füge jeder Zeile das Argument `discard` an und speichere die Datei.

Erstelle das initramfs neu:

	:::bash
	sudo dracut -f

Überprüfe, ob alles geklappt hat:

	:::bash
	sudo fstrim -v /

Aktiviere eine wöchentlich wiederkehrende Aufgabe über systemd:

	:::bash
	sudo systemctl enable fstrim.timer

[1]: https://blog.christophersmart.com/2016/05/12/trim-on-lvm-on-luks-on-ssd-revisited/
     "Christopher Smart's Blog"
