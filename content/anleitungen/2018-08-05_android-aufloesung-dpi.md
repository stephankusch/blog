Title: Auflösung und Pixeldichte unter Android ändern
Date: 2018-08-05T14:32+02:00
Modified: 2018-08-05T14:32+02:00
Category: Anleitungen
Tags: Android, ADB
Slug: android-aufloesung-dpi
Authors: Stephan Kusch
Status: published

Auch unter Android lässt sich die Auflösung ändern. Damit die Bildinhalte wie
gewohnt erscheinen, sollte im gleichen Atemzug auch die Pixeldichte im gleichen
Verhältnis geändert werden. Alternativ kann über die Pixeldichte natürlich auch
die Skalierung der Bildinhalte gesteuert werden. Dabei führt ein größerer Wert
zu größeren Inhalten und umgekehrt.

Benötigt wird ein Android mit aktiviertem Debugging und auf Rechnerseite ein
installiertes ADB.

Die Auflösung ändert man so exemplarisch auf 1080x1920:

	:::bash
	adb shell wm size 1080x1920

Die Pixeldichte ändert man so exemplarisch auf 440 DPI:

	:::bash
	adb shell wm density 440

