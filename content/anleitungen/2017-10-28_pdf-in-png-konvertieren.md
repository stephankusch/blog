Title: PDF-Datei in PNG-Datei konvertieren
Date: 2017-10-28T14:24+02:00
Modified: 2017-10-28T14:24+02:00
Category: Anleitungen
Tags: Ghostscript, PDF
Slug: pdf-in-png-konvertieren
Authors: Stephan Kusch
Status: published

Hier findest du eine Quick-'n'-Dirty-Lösung zum Konvertieren eines PDF in ein
PNG mittels Ghostscript [Quelle][1]:

	:::bash
	gs -sDEVICE=pngalpha -sOutputFile=name.png name.pdf

[1]: https://stackoverflow.com/questions/653380/converting-a-pdf-to-png#3379649
     "Converting a PDF to PNG - Stack Overflow"
