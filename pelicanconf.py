#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

# Basic settings
AUTHOR = 'Stephan Kusch'
SITENAME = 'Memos'
SITEURL = ''

PATH = 'content/'
DELETE_OUTPUT_DIRECTORY = True
OUTPUT_PATH = 'public/'
PAGE_PATHS = ['pages']
STATIC_PATHS = ['images']

TIMEZONE = 'Europe/Berlin'
DEFAULT_LANG = 'de'

USE_FOLDER_AS_CATEGORY = True
DEFAULT_CATEGORY = 'Andere'
DISPLAY_PAGES_ON_MENU = True
DISPLAY_CATEGORIES_ON_MENU = True

DEFAULT_DATE = '1970-01-01T00:00+00:00'
DEFAULT_DATE_FORMAT = '%A, %x, %H:%M'

SUMMARY_MAX_LENGTH = 32

CACHE_CONTENT = False
LOAD_CONTENT_CACHE = False

# URL settings
ARTICLE_URL = 'posts/{date:%Y}/{date:%m}/{date:%d}/{slug}/'
ARTICLE_SAVE_AS = 'posts/{date:%Y}/{date:%m}/{date:%d}/{slug}/index.html'
PAGE_URL = 'pages/{slug}/'
PAGE_SAVE_AS = 'pages/{slug}/index.html'
CATEGORY_URL = 'category/{slug}/'
CATEGORY_SAVE_AS = 'category/{slug}/index.html'
TAG_URL = 'tag/{slug}/'
TAG_SAVE_AS = 'tag/{slug}/index.html'
#AUTHOR_URL = 'author/{author}/'
#AUTHOR_SAVE_AS = 'author/{author}/index.html'
YEAR_ARCHIVE_SAVE_AS = 'posts/{date:%Y}/index.html'
MONTH_ARCHIVE_SAVE_AS = 'posts/{date:%Y}/{date:%m}/index.html'
DAY_ARCHIVE_SAVE_AS = ''

# Theme settings

THEME = 'themes/alchemy'

SITESUBTITLE = 'Meine Gedankenstütze'
SITEIMAGE = '/images/profile.png width=256 height=256'
DESCRIPTION = 'Mein persönliches Weblog.'

LINKS = (
    ('Pelican', 'http://getpelican.com/'),
    ('Python.org', 'http://python.org/'),
    ('Jinja2', 'http://jinja.pocoo.org/'),
)

ICONS = [
    ('gitlab', 'https://gitlab.com/stephankusch/blog'),
]

PYGMENTS_STYLE = 'monokai'
HIDE_AUTHORS = True
RFG_FAVICONS = False

# Default value is ['index', 'tags', 'categories', 'authors', 'archives']
DIRECT_TEMPLATES = ['index', 'tags', 'categories', 'authors', 'archives', 'sitemap']
SITEMAP_SAVE_AS = 'sitemap.xml'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Social widget
#SOCIAL = ()

# Pagination settings
DEFAULT_PAGINATION = 10
PAGINATION_PATTERNS = ((1, '{base_name}/', '{base_name}/index.html'),
                       (2, '{base_name}/page/{number}/', '{base_name}/page/{number}/index.html'),)

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True
